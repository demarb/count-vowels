# count_vowels.py
# Programmer: Demar Brown (Demar Codes)
# Date: Jan 1, 2021
# Program Details: This program has a user enter a string.
                    #The program counts the number of vowels in the text and prints it out.
                    #For added complexity, the program reports the sum of each vowel found.

#-------------------
# ------------------------------
def vowel_count(user_string):

    total_vowels= 0
    vowel_dict = {
        "a" : 0,
        "e" : 0,
        "i" : 0,
        "o" : 0,
        "u" : 0
    }

    user_string = user_string.lower()
    user_input = []

    for charac in user_string:
        user_input.append(charac)

    # print(user_input)

    for item in vowel_dict.keys():
        for letter in user_string:
            if item == letter:
                vowel_dict[item] = vowel_dict[item] + 1

    # print(vowel_dict.items())

    for val in vowel_dict.values():
        total_vowels += val

    print("\nA's found: "+ str(vowel_dict.get("a")))
    print("\nE's found: "+ str(vowel_dict.get("e")))
    print("\ni's found: "+ str(vowel_dict.get("i")))
    print("\nO's found: "+ str(vowel_dict.get("o")))
    print("\nU's found: "+ str(vowel_dict.get("u")))
    print("\n\nTotal Vowels found: "+ str(total_vowels))

#Program starts here

user_string =  input("\nThis is a Vowel Checker. Enter your string here: ")
vowel_count(user_string)